<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500;600;700;800&display=swap"
        rel="stylesheet">
    <!-- Style -->
    <link rel="stylesheet" href="{{asset('assets')}}/css/style.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/responsive.css">
    <!-- Vendor -->
    <link rel="stylesheet" href="{{asset('assets')}}/vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/vendor/swiper/swiper-bundle.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <title>Moestopo Academy</title>
</head>

<body>
    @include('components.include.navbar')

    @yield('content')

    @include('components.include.footer')

    <script src="{{asset('assets')}}/vendor/bootstrap/bootstrap.bundle.min.js"></script>
    <script src="{{asset('assets')}}/vendor/swiper/swiper-bundle.min.js"></script>
    @stack('scripts')

</body>

</html>