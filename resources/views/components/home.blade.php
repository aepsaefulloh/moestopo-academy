@extends('master')

@section('content')

<section class="section-banner">
    <img src="{{asset('assets')}}/images/banner/banner.png" class="img-fluid w-100" alt="banner">
</section>

<section class="section-about section-space">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="heading">
                    <div class="title pallet">about us</div>
                    <div class="subtitle">PROGRAM PENINGKATAN KOMPETENSI PENDIDIKAN VOKASIONAL</div>
                </div>
                <div class="content">
                    <p>
                        Program sertifikasi digital skill berstandar internasional untuk siswa SMK di seluruh
                        Indonesia dengan harapan menyiapkan talenta muda yang sesuai dengan kebutuhan industri
                        digital.

                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec
                        ullamcorper mattis, pulvinar dapibus leo. Dolor sit amet, consectetur adipiscing elit.
                    </p>
                </div>
            </div>
            <div class="col-md-6 offset-md-1">
                <div class="about-image">
                    <img src="{{asset('assets')}}/images/about.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-vision section-space">
    <div class="container">
        <div class="row reverse">
            <div class="col-md-6">
                <div class="vision-image mt-5 mt-md-0">
                    <img src="{{asset('assets')}}/images/vision.png" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="heading">
                    <div class="title pallet">about us</div>
                    <div class="subtitle">Our Great Vision & Mission</div>
                </div>
                <div class="content" style="color:#5A5A5A;">
                    <h3>Vision</h3>
                    <p>
                        Terwujudnya Fakultas Ilmu Komunikasi yang unggul, berdaya saing, bercitra positif serta
                        berwawasan kebangsaan dan menjadikan program studi berkelas Internasional pada tahun 2032
                    </p>
                    <h3>Mission</h3>
                    <div class="pt-3">

                        <div class="box-vision">
                            <div class="box-icon">
                                1.
                            </div>
                            <div class="box-content ms-3">
                                <div class="caption">Menyelenggarakan pendidikan berkualitas dan berdaya saing yang
                                    mampu menghasilkan lulusan unggul dan mampu berkompetisi di dunia kerja.</div>
                            </div>
                        </div>
                        <div class="box-vision">
                            <div class="box-icon">
                                2.
                            </div>
                            <div class="box-content ms-3">
                                <div class="caption">Menetapkan keunggulan, daya saing dan citra positif, sebagai nilai
                                    dasar dalam melaksanakan pendidikan, penelitian dan pengabdian kepada masyarakat.
                                </div>
                            </div>
                        </div>
                        <div class="box-vision">
                            <div class="box-icon">
                                3.
                            </div>
                            <div class="box-content ms-3">
                                <div class="caption">Menetetapkan wawasan kebangsaan sebagai landasan utama dalam
                                    membangun karakter kejuangan civitas akademika menuju persaingan global dan
                                    regional.</div>
                            </div>
                        </div>
                        <div class="box-vision">
                            <div class="box-icon">
                                4.
                            </div>
                            <div class="box-content ms-3">
                                <div class="caption">Membangun suasana belajar yang kondusif sehingga mendukung dan
                                    sejalan dengan terselenggaranya prinsip keunggulan, daya saing serta citra positif
                                    guna mendukung terwujudnya Fakultas Ilmu Komunikasi berkelas internasional.</div>
                            </div>
                        </div>
                        <div class="box-vision">
                            <div class="box-icon">
                                5.
                            </div>
                            <div class="box-content ms-3">
                                <div class="caption">Menyelenggarakan penelitian dan Pengabdian kepada Masyarakat sesuai
                                    dengan keilmuan dan penerapannya.</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-mentor section-space">
    <div class="container">
        <div class="row ">
            <div class="col-12">
                <div class="heading text-center">
                    <div class="title pallet">Our Mentor</div>
                    <div class="subtitle">OUR GREAT TEACHER</div>
                </div>
            </div>
            <div class="col-12">
                <div class="swiper mentorSlider">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <img src="{{asset('assets')}}/images/mentor/mentor-1.png" class="img-fluid" alt="">
                            <div class="mentor-content">
                                <div class="name">Bernarr Dominik</div>
                                <div class="description">Founder</div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <img src="{{asset('assets')}}/images/mentor/mentor-2.png" class="img-fluid" alt="">
                            <div class="mentor-content">
                                <div class="name">Bernarr Dominik</div>
                                <div class="description">Founder</div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <img src="{{asset('assets')}}/images/mentor/mentor-3.png" class="img-fluid" alt="">
                            <div class="mentor-content">
                                <div class="name">Bernarr Dominik</div>
                                <div class="description">Founder</div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <img src="{{asset('assets')}}/images/mentor/mentor-4.png" class="img-fluid" alt="">
                            <div class="mentor-content">
                                <div class="name">Bernarr Dominik</div>
                                <div class="description">Founder</div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-contact">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="heading">
                    <div class="title">Ingin Daftar Fikom Moestopo?</div>
                    <div class="subtitle">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consectetur non
                        metus nun</div>
                </div>
            </div>
            <div class="col-md-6 align-self-md-center text-md-end">
                <a href="{{url('/register')}}" class="btn btn-primary p-3  mt-4 mt-md-0">Register Now</a>
            </div>
        </div>
    </div>
</section>

@endsection

@push('scripts')
<script>
var swiper = new Swiper(".mentorSlider", {
    slidesPerView: 2,
    loop: true,
    spaceBetween: 10,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    breakpoints: {
        768: {
            slidesPerView: 4,
            spaceBetween: 40,
        },
    },
});
</script>
@endpush