<footer>
    <section class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-3 mb-4">
                    <h6 class="text-uppercase fw-bold mb-4">
                        Company name
                    </h6>
                    <p>
                        Here you can use rows and columns to organize your footer content. Lorem ipsum
                        dolor sit amet, consectetur adipisicing elit.
                    </p>
                    <div class="social-menu">
                        <ul>
                            <li>
                                <a href="" target="blank">
                                    <img src="{{asset('assets')}}/images/facebook.png" class="img-fluid" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="" target="blank">
                                    <img src="{{asset('assets')}}/images/instagram.png" class="img-fluid" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="" target="blank">
                                    <img src="{{asset('assets')}}/images/twitter.png" class="img-fluid" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="" target="blank">
                                    <img src="{{asset('assets')}}/images/youtube.png" class="img-fluid" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-6 col-md-2 mb-4">
                    <h6 class="text-uppercase fw-bold mb-4">
                        Links
                    </h6>
                    <p>
                        <a href="#!" class="">Home</a>
                    </p>
                    <p>
                        <a href="#!" class="">About</a>
                    </p>
                    <p>
                        <a href="#!" class="">Our Mission</a>
                    </p>
                    <p>
                        <a href="#!" class="">Registration</a>
                    </p>
                    <p>
                        <a href="#!" class="">Contact</a>
                    </p>

                </div>

                <div class="col-6 col-md-2 mb-4">
                    <h6 class="text-uppercase fw-bold mb-4">
                        About
                    </h6>
                    <p>
                        <a href="#!" class="">Mentors</a>
                    </p>
                    <p>
                        <a href="#!" class="">Careers</a>
                    </p>
                    <p>
                        <a href="#!" class="">Press</a>
                    </p>
                    <p>
                        <a href="#!" class="">Community</a>
                    </p>
                </div>

                <div class="col-md-2 mb-4">
                    <h6 class="text-uppercase fw-bold mb-4">
                        Community
                    </h6>
                    <p>
                        <a href="#!" class="">Events</a>
                    </p>
                    <p>
                        <a href="#!" class="">Blog</a>
                    </p>
                    <p>
                        <a href="#!" class="">Forum</a>
                    </p>
                </div>

                <div class="col-md-3 mb-4">
                    <h6 class="text-uppercase fw-bold mb-4">
                        Our Office
                    </h6>
                    <p>
                        <a href="">Indonesia
                            Jl. Tebet Timur Dalam Raya No.37, RW.1, Tebet Tim., Kec. Tebet, Kota Jakarta Selatan,
                            Daerah Khusus Ibukota Jakarta 12820</a>
                    </p>
                </div>


            </div>
            <hr style="border-top:1px solid #969696">

        </div>

    </section>
    <div class="text-center p-4">
        © © 2021 Nusantara All rights reserved.
    </div>
</footer>