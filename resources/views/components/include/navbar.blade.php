<section class="section-navbar">
    <nav class="navbar navbar-expand-lg bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{url('/')}}">
                <img src="{{asset('assets')}}/images/logo.png" alt="logo universitas moestopo" width="60">
                <span class="logo-title d-none d-md-inline">Universitas FIKOM MOESTOPO</span>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ms-md-auto">
                    <a class="nav-link active" aria-current="page" href="{{url('/')}}">Home</a>
                    <a class="nav-link" href="#">Mission</a>
                    <a class="nav-link" href="#">Program</a>
                    <a class="nav-link" href="#">Join Us</a>
                    <a href="{{url('/register')}}" class="btn btn-outline-primary px-3 ms-2">Register</a>
                </div>
            </div>
        </div>
    </nav>
</section>