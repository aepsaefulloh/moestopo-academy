@extends('master')

@section('content')


<section class="section-auth section-space">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="{{asset('assets')}}/images/auth.png" class="img-fluid" alt="">
            </div>
            <div class="col-md-6">
                <h2>Daftar Univ Sekarang</h2>
                <p>Daftarkan Dirimu Sekarang Juga</p>
                <form>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label">Nama</label>
                                <input type="text" class="form-control" placeholder="Joni">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label">Email</label>
                                <input type="email" class="form-control" placeholder="name@example.com">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mb-3">
                                <label class="form-label">Nomor</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="checklist1">
                                <label class="form-check-label" for="checklist1">
                                    Saya Ingin DI telpon oleh tim Univ
                                </label>
                            </div>
                            <div class="form-check my-4">
                                <input class="form-check-input" type="checkbox" value="" id="checklist2">
                                <label class="form-check-label" for="checklist2">
                                    Saya Setuju dengan semua <a href="#">Syarat Dan Ketentuan</a>
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-submit w-100 py-3">Login</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>



@endsection